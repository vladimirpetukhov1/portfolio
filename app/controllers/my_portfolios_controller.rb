class MyPortfoliosController < ApplicationController
  def index
    @my_portfolios = MyPortfolio.all
  end

  def new
    @my_portfolio = MyPortfolio.new
    3.times { @my_portfolio.technologies.build }
  end

  def filter
    filter = params[:items]
    @my_portfolios = MyPortfolio.with_subtitle(items)
  end

  # GET /blogs/1 or /blogs/1.json
  def show
    @my_portfolio = MyPortfolio.find(params[:id])
  end

  # GET /my_portfolio/1/edit
  def edit
    @my_portfolio = MyPortfolio.find(params[:id])
  end

  # DELETE /blogs/1 or /blogs/1.json
  def destroy
    @my_portfolio = MyPortfolio.find(params[:id])
    @my_portfolio.destroy
    respond_to do |format|
      format.html { redirect_to my_portfolios_path, notice: 'Portfolio was successfully removed.' }
    end
  end

  # POST /my_portfolio
  def create
    @my_portfolio = MyPortfolio.new(params.require(:my_portfolio).permit(:title, :subtitle, :body, 
      technologies_attributes: [:name]))

    respond_to do |format|
      if @my_portfolio.save
        format.html { redirect_to my_portfolios_path, notice: 'Portfolio was successfully created.' }
      else
        format.html { render :new, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /my_portfolios/1
  def update
    @my_portfolio = MyPortfolio.find(params[:id])
    respond_to do |format|
      if @my_portfolio.update(params.require(:my_portfolio).permit(:title, :subtitle, :body))
        format.html { redirect_to my_portfolios_path, notice: 'Portfolio was successfully updated.' }
      else
        format.html { render :edit, status: :unprocessable_entity }
      end
    end
  end
end
