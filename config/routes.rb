Rails.application.routes.draw do
  
  devise_for :users, path: '', path_names: { sign_in: 'login', sign_out: 'logout', sign_up: 'register' }
  resources :my_portfolios, except: [:show]
  get 'my_portfolio/:id', to: 'my_portfolios#show', as: 'my_portfolio_show'
  get 'my_portfolio/:id/edit', to: 'my_portfolios#show', as: 'my_portfolio_edit'
  delete 'my_portfolio/:id', to: 'my_portfolios#destroy', as: 'my_portfolio_delete'
  

  get 'about-me', to: 'pages#about'
  get 'contact', to: 'pages#contact'

  resources :blogs do
    member do
      get :toggle_status
    end
  end

  root to: 'pages#home'
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html
end
